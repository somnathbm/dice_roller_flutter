import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  // initialize state
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  // declare local state variables
  String descriptionText = 'This text will be changed';
  String diceImage = 'assets/dice.png';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Roll Dice',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Roll Dice'),
          backgroundColor: Colors.green
        ),
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('$diceImage'),
              RaisedButton(
                child: Text('Tap to Roll Dice'),
                color: Colors.black,
                textColor: Colors.white,
                onPressed: () => handleRoll(),
              )
            ],
          )
        ),
      ),
    );
  }

  handleRoll() {
    //var rand = Random();
    var randInt = 1 + Random().nextInt(7-1);
    setState(() {
      diceImage = 'assets/dice-$randInt.png';
    });
  }
}
